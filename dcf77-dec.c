#include <stdio.h>
#include <stddef.h>



//char data_t[] = "00111100010001000010100100001000010101000101010000110010001";
char data_t[] = "00101110110100100010100000101000010101000101010000110011001";
/* 
   0-01111000100010-000101-00100001-0000101-010001-010-10000-110010001
22.01.2013 10:04

0-01111000100010- Res
0 R
0 A1
01 Z1,2
0 A2
1- S
001 0000

1-0000101-010001-010-10000-110010001


*/
typedef struct _s77 {
  unsigned int summer:1;
  unsigned int cest:1;
  unsigned int cet:1;
  unsigned int min:7;
  unsigned int P1:1;
  unsigned int hour:6;
  unsigned int P2:1;
  unsigned int day:6;
  unsigned int dow:3;
  unsigned int month:5;
  unsigned int year:8;
  unsigned int P3:1;
  unsigned int calc_P1:1;
  unsigned int calc_P2:1;
  unsigned int calc_P3:1;
  unsigned int valid:1;
} DCF77;


#define BCD_DEC(x)    ( (x & 0x0F) + 10*((x & 0xF0)>>4))

unsigned char SET_FIELD(char *data, int start, int w)
{
  unsigned char b = 0;
  int x;

  unsigned char mask=1;
  data += start;// point to the last one
  while(w--) {
    if (*data == '1') {
      b |= mask;

    }
    mask <<= 1;
    data++;
  }

  return b;
  
}
unsigned char CHECK_PAR(char *data, int start, int w)
{
  unsigned char rc = 0;
  data += start;
  while(w--) {
    if(*data == '1') {
      rc ^= 1;
    }
    data++;
  }
  return rc;
}

void fill_dcf(DCF77 *dcf, char *data)
{
  memset(dcf,0,sizeof(*dcf));
  dcf->cest = SET_FIELD(data,17,1);
  dcf->cet  = SET_FIELD(data,18,1);
  dcf->min  = SET_FIELD(data,21,7);
  dcf->hour = SET_FIELD(data,29,6);
  dcf->day =  SET_FIELD(data,36,6);
  dcf->year = SET_FIELD(data,50,8);
  dcf->dow  = SET_FIELD(data,42,3);
  dcf->P1   = SET_FIELD(data,28,1);
  dcf->P2   = SET_FIELD(data,35,1);
  dcf->P3   = SET_FIELD(data,58,1);
  dcf->calc_P1 = CHECK_PAR(data,21,7);
  dcf->calc_P2 = CHECK_PAR(data,29,6);
  dcf->calc_P3 = CHECK_PAR(data,36,22);
  if (dcf->P1 == dcf->calc_P1 &&
      dcf->P2 == dcf->calc_P2 &&
      dcf->P3 == dcf->calc_P3 &&
      dcf->cest != dcf->cet) {
    dcf->valid = 1;
  } else {
    dcf->valid = 0;
  }
}

main()
{
  int x=0;
  int bit=8;
  int y=0;
  unsigned int a,b;
  DCF77 dcf;
  fill_dcf(&dcf, data_t);
  

  printf("Done!  hour=%d min=%d day=%d month=%d year=%d  %s\n",

	 BCD_DEC(dcf.hour),
	 BCD_DEC(dcf.min),
	 BCD_DEC(dcf.day),
	 BCD_DEC(dcf.month)+1,
	 BCD_DEC(dcf.year)+2000,
	 dcf.valid ? " VALID " : "****NOT VALID****"
);
    return 0;
  }
